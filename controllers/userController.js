const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");



// Checking if the email exists in the database
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {
		return result.length > 0;
	});

}


module.exports.registerUser = (reqBody) => {

	let newUser = new User({

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// bcrypt.hashSync(<dataToBeHashed>, <saltRound>)
		password: bcrypt.hashSync(reqBody.password, 10)

	});

	return newUser.save().then((user, error) => {
		return error ? false : user;
	});

}


module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			return isPasswordCorrect ? { access: auth.createAccessToken(result) } : false;
		}
	})

}


module.exports.getUser = (reqBody) => {

	return User.findById(reqBody.id).then(result => {
		if (result == null) {
			return false;
		} else {
			result.password = "";
			return result;
		}
	});

}