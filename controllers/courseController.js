const Course = require("../models/Course");
const auth = require("../auth");

module.exports.addCourse = (data) => {

	if (data.isAdmin) {
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		});

		return newCourse.save().then((course, error) => {
			return error ? false : true;
		});
	} else {
		let message = Promise.resolve("User must be ADMIN to access this");
		return message.then(value => {
			return {value};
		});
	}

}


// Retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	});
}


// Retrieve all the active courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
}


// Retrieve specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
}


// Update course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	/*
		Syntax:
		- findByIdAndUpdate(document ID, updatesToBeApplied)
	*/
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course, error) => {
		return error ? false : true;
	});
}


// Archive a course
module.exports.archiveCourse = (reqParams, reqBody) => {
	return Course.findByIdAndUpdate(reqParams.courseId, {isActive: reqBody.isActive}).then((course, error) => {
		return error ? false : true;
	});
}