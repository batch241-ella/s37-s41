const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");


// Route for checking if the user's email already exists in our database
router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(resultFromController => {
		res.send(resultFromController);
	});

});


// Route for User Registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => {
		res.send(resultFromController);
	});

});

// Route for User Authentication
router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));

});

// Route for Retrieving User details
router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	/*
		{
		  id: '63dc65f62a9fcb8b61139437',
		  email: 'john@mail.com',
		  isAdmin: false,
		  iat: 1675402546
		}
	*/

	userController.getUser({id: userData.id}).then(resultFromController => {
		res.send(resultFromController);
	});

});


module.exports = router;