const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";


module.exports.createAccessToken = (user) => {
	// When the user logs in, a token will be created with user's information

	// {
	//   _id: new ObjectId("63dc65ad54cd7478899ffb37"),
	//   firstName: 'John',
	//   lastName: 'Smith',
	//   email: 'john@mail.com',
	//   password: '$2b$10$dZ1SxxY/PPyK3J.2qhbVL.dAj1lo/YK6z0cnjri0d7NLGnJ3rwwUu',
	//   isAdmin: false,
	//   mobileNo: '09123456789',
	//   enrollments: [],
	//   __v: 0
	// }

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	// jwt.sign(data/payload, secretKey, options) 
	return jwt.sign(data, secret, {});

}



// Token Verification
module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;
	/*
		token is: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzZGM2NWFkNTRjZDc0Nzg4OTlmZmIzNyIsImV...'
		when decoded gives: 
			{
			  id: '63dc65f62a9fcb8b61139437',
			  email: 'john@mail.com',
			  isAdmin: false,
			  iat: 1675402546
			}
	*/

	if (typeof token !== "undefined") {

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {
			if (error) {
				return res.send({auth: "failed"});
			} else {
				next();
			}
		});

	} else {
		return res.send({auth: "failed"});
	}

}


// Token Decryption 
module.exports.decode = (token) => {

	if (typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {

			if (error) {
				return null;
			} else {
				return jwt.decode(token, {complete: true}).payload;
			}

		});

	} else {
		return null;
	}

}
